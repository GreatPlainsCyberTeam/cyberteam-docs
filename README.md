Live site can be viewed here: https://greatplainscyberteam.gitlab.io/cyberteam-docs

Made with MkDocs: https://www.mkdocs.org

To modify the site:

 1. Install mkdocs on your local machine by following the installation instructions 
[here](https://www.mkdocs.org/#installation)
 2. Clone this repo
 3. Preview the site with the command `mkdocs serve`

Customized for GitLab by matching this repo: https://gitlab.com/pages/mkdocs/

Random Notes:

### Change the theme

Remove the following line from `mkdocs.yml` to switch from the readthedocs to the 
default theme:

	theme: readthedocs

### Changing the Favicon Icon

By default, MkDocs uses the MkDocs favicon icon. To use a different icon, create 
an img subdirectory in your docs_dir and copy your custom favicon.ico file to that 
directory. MkDocs will automatically detect and use that file as your favicon icon.
